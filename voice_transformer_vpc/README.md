# COMPRISE voice transformation based on the baseline of the Voice Privacy Challenge 2020

This voice conversion method requires kaldi and nii. We provide a docker image with these tools pre-installed.  
Please visit the [challenge website](https://www.voiceprivacychallenge.org/) for more information about the Challenge.

## Quick Start with Docker

Inputs, models and config must be in the `io` folder that will be mounted as a volume to the docker container.

### Build the params

From this repo, run :

```
docker run --gpus all \
  -v "$(pwd)"/io:/opt/io \
  registry.gitlab.inria.fr/comprise/development/vpc-transformer \
  ./build.sh --anoni_pool train-other-500
```

This will download the `train-other-500` subset of Librispeech and extract the x-vectors of the speakers of this dataset. These x-vectors will be used to create a Transformer in the next step.

### Running a transformation sample

From this repo, run :

```
docker run --gpus all \
  -v "$(pwd)"/io:/opt/io \
  registry.gitlab.inria.fr/comprise/development/vpc-transformer \
  ./transform.sh --ipath io/inputs/e0003.wav
```

The output will be in `io/results`.

The transformation uses the provided pre-trained models of anonymization and a specific configuration file that sets some transformation parameters (`io/config/config_params.sh`).

You can also run the container in interactive mode :

```
docker run -it --gpus all \
  -v "$(pwd)"/io:/opt/io \
  registry.gitlab.inria.fr/comprise/development/vpc-transformer
```

## Configuration and parameters

Main parameter for `transform.sh`: 

- `--ipath` input path for the wav file to transform (wav format : RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 16000 Hz) 

- `--opath` output path: default is results

The file `config_transform.sh` contains the parameters of the transformation that can also be given on the command line. 

```
wgender=f                             # gender m or f
pseudo_xvec_rand_level=spk            # spk (all utterances will have same xvector) or utt (each utterance will have randomly selected xvector)
#cross_gender="same"                  # false, same gender xvectors will be selected; true, other gender xvectors
#cross_gender="other"                 # false, same gender xvectors will be selected; true, other gender xvectors; random gender can be selected
cross_gender="random"                 # false, same gender xvectors will be selected; true, other gender xvectors; random gender can be selected
distance="plda"                       # cosine or plda
#proximity="random"                   # nearest or farthest speaker to be selected for anonymization
#proximity="farthest"                 # nearest or farthest speaker to be selected for anonymization
proximity="dense"                     # nearest or farthest speaker to be selected for anonymization
anoni_pool="train_other_500"          # change this to the data you want to use for anonymization pool
```


## Manual installation

### Install

1. Clone this repo
2. You must install some dependencies using `sudo` or `sudo-g5k` on grid5000. `sudo apt install sox flac`, and `cd kaldi/tools && sudo extras/install_mkl.sh`.
3. ./install.sh

### Running the recipe

1. `cd vpc` 
2. run `./build.sh` and `transform.sh`. 

## General information

For more details about the baseline and data, please see [The VoicePrivacy 2020 Challenge Evaluation Plan](https://www.voiceprivacychallenge.org/docs/VoicePrivacy_2020_Eval_Plan_v1_1.pdf)

For the latest updates in the baseline and evaluation scripts, please visit [News and updates page](https://github.com/Voice-Privacy-Challenge/Voice-Privacy-Challenge-2020/wiki/News-and-Updates)


## Building the docker image and running in a interactive mode

```
git clone --recurse-submodules https://gitlab.inria.fr/comprise/development/vpc-transformer  
cd vpc-transformer
sudo docker build -t comprise-vpc .
sudo docker run -it --gpus all  \
	-v "$(pwd)"/io:/opt/io \  
	comprise-vpc
./transform.sh --ipath ./inputs/e0003.wav
```


## License

Copyright (C) 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

---------------------------------------------------------------------------
