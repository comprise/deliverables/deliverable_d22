import torch
import torch.nn as nn
import torch.nn.functional as F
from si_models import SILinear, SIConv2d

import torch
from torchvision import datasets
from torch.utils.data import DataLoader, random_split, RandomSampler
import torchvision.transforms as transforms
import numpy as np
from tqdm import tqdm


class LeNet5(nn.Module):
    def __init__(self, normalization_type):
        super(LeNet5, self).__init__()
        self.normalization_type = normalization_type
        self.conv1 =SIConv2d(in_channels=1, out_channels=6, kernel_size=5, stride=1, padding=2)
        self.max_pool_1 = torch.nn.MaxPool2d(kernel_size=2)
        self.conv2 = SIConv2d(in_channels=6, out_channels=16, kernel_size=5, stride=1, padding=0)
        self.max_pool_2 = torch.nn.MaxPool2d(kernel_size=2)
        self.fc1 = SILinear(16*5*5, 120)
        self.fc2 = SILinear(120, 84)
        self.fc3 = SILinear(84, 10)

        if normalization_type == 'Layer':
            self.n1 = nn.LayerNorm([6, 28, 28], elementwise_affine=False)
            self.n2 = nn.LayerNorm([16, 10, 10], elementwise_affine=False)
            self.n3 = nn.LayerNorm(120, elementwise_affine=False)
            self.n4 = nn.LayerNorm(84, elementwise_affine=False)
        elif normalization_type == 'Batch':
            self.n1 = nn.BatchNorm2d(6, affine=False, track_running_stats=False)
            self.n2 = nn.BatchNorm2d(16, affine=False, track_running_stats=False)
            self.n3 = nn.BatchNorm1d(120, affine=False, track_running_stats=False)
            self.n4 = nn.BatchNorm1d(84, affine=False, track_running_stats=False)


    def forward(self, x, noise_std):
        x = F.relu(self.conv1(x, noise_std))

        if self.normalization_type is not None:
            x = self.n1(x)

        x = self.max_pool_1(x)
        x = F.relu(self.conv2(x, noise_std))

        if self.normalization_type is not None:
            x = self.n2(x)

        x = self.max_pool_2(x)
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x, noise_std))

        if self.normalization_type is not None:
            x = self.n3(x)

        x = F.relu(self.fc2(x, noise_std))
        if self.normalization_type is not None:
            x = self.n4(x)

        x = self.fc3(x, noise_std)

        return x


def train(model, criterion, optimizer, device, train_loader, clip, noise_multiplier, batch_size, axi_x):
    model.train()
    total_diff = 0.0
    for x, y in tqdm(train_loader):
        _lr = optimizer.param_groups[0]['lr']
        std_params = _lr * clip * noise_multiplier / batch_size
        x = torch.cat([x.to(device), axi_x], dim=0)
        y = y.to(device)

        optimizer.zero_grad()
        output = model.forward(x, std_params)[:batch_size]
        losses = criterion(output, y)

        saved_var = dict()
        for tensor_name, tensor in model.named_parameters():
            saved_var[tensor_name] = torch.zeros_like(tensor)

        for j in losses:
            j.backward(retain_graph=True)
            torch.nn.utils.clip_grad_norm_(model.parameters(), clip)
            for tensor_name, tensor in model.named_parameters():
                new_grad = tensor.grad
                saved_var[tensor_name].add_(new_grad)
            optimizer.zero_grad()

        for tensor_name, tensor in model.named_parameters():
            tensor.grad = saved_var[tensor_name] / losses.shape[0]

        optimizer.step()


def evaluate_accuracy(model, data_loader, device, axi_x):
    model.eval()
    correct = 0
    total = 0.
    with torch.no_grad():
        for x, y in data_loader:
            _batch_size = x.shape[0]
            x = torch.cat([x.to(device), axi_x], dim=0)
            output = model(x, 0)[:_batch_size]
            pred = output.max(1, keepdim=True)[1]  # get the index of the max log-probability
            correct += pred.eq(y.to(device).view_as(pred)).sum().item()
            total += _batch_size
    accuracy = correct / total
    return accuracy

def main():
    epsilon = 0.05
    normalization_type = 'Batch'
    noise_multiplier = 7.1
    clip = 3.0
    max_lr = 0.1
    batch_size = 32
    epochs = 10
    gamma = 0.9
    device = 'cuda:0'



    model = LeNet5(normalization_type)
    model = model.to(device)
    criterion = torch.nn.CrossEntropyLoss(reduction='none')
    optimizer = torch.optim.SGD(model.parameters(), lr=max_lr, momentum=0.9)
    scheduler_lr = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma, last_epoch=-1)



    transform = transforms.Compose([transforms.ToTensor()])
    train_data = datasets.MNIST(root='./data', train=True, download=True, transform=transform)
    test_data = datasets.MNIST(root='./data', train=False, download=True, transform=transform)
    sampler = RandomSampler(train_data, replacement=True)
    train_loader = DataLoader(train_data, batch_size, num_workers=4, sampler=sampler, drop_last=True)
    test_loader = DataLoader(test_data, batch_size=1)


    axi_data = datasets.KMNIST(root='./data', train=True, download=True, transform=transform)
    axi_loader = torch.utils.data.DataLoader(axi_data, batch_size=200)
    for x, y in axi_loader:
        axi_x = x[:30].to(device)
        break

    for epoch in range(epochs):
        train(model, criterion, optimizer, device, train_loader, clip, noise_multiplier, batch_size, axi_x)
        scheduler_lr.step()
        test_accuracy = evaluate_accuracy(model, test_loader, device, axi_x)

        print('epoch', epoch)
        print('test_accuracy', test_accuracy)
        print('---------------------')



if __name__ == '__main__':
    main()












